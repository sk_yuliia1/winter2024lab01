public class Hangman{
	//This method asks the user to enter an input, that is string
	//If the input is more than 4 characters long, the program will end
	public static void main (String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("NOTE: You have a maximum of 6 attempts to guess");
		System.out.println("Enter a 4 letter word: ");
		String word = reader.next();
		if (word.length()>4){
			System.out.println("You must enter 4 letter word!");
			} else {
		runGame(word); //Calls runGame method which takes as a variable a string entered previously
	}
}

	//This method takes in as the parameter String and char
	//It returns the position of the letter in the guessed word
	//Also, if the entered char isn't a part of the guessed word, it returns -1
	public static int isLetterInWord(String word, char c){
		int indexOfLetter = 0;
		while(indexOfLetter < 4) {
			if (c == word.charAt(indexOfLetter)){ //Checks if char C is part of a guessed word
				return indexOfLetter;
			} else {
				indexOfLetter++;
			}
		} return -1;
	}
	
	//This method takes in as a parameter a String and 4 booleans related to letters from 1 to 4
	//It prints a letter if the letter's index matches its real position in the guessed word
	//Otherwise, it will just output '_' 
	//public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
	public static void printWord(String word, boolean[] letters){
		for(int i = 0; i<word.length(); i++){
			if(i == 0 && letters[0] == true){
				System.out.print(word.charAt(i));
			} else if (i == 1 && letters[1] == true){
				System.out.print(word.charAt(i));
			
			} else if (i == 2 && letters[2] == true){
				System.out.print(word.charAt(i));
			
			} else if (i == 3 && letters[3] == true){
				System.out.print(word.charAt(i));
			
			} else { 
				System.out.print("_");
			}
		}
	}
	
	//This method takes in a string word as a parameter 
	//This method checks the existence of the entered characters in the word according to the index obtained by the isLetterInWord method
	//Otherwise, it prints the number of failed attempts
	 public static void runGame(String word){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		boolean[] letters = new boolean[4];
		for(int i = 0; i < letters.length-1; i++){
			letters[i] = false; 
		}
		int misses = 0; //this variable shows the number of failed attempts
		
		// while runs until all these boolean values exist together and the misses value is less than 6 
		while(!(letters[0] && letters[1] && letters[2] && letters[3]) && misses < 6){
			System.out.println("Enter a letter: "); 
			//Stores user's input in a char variable 
			char c = reader.next().charAt(0); 
			//Stores the result of isLetterInWord method in a variable that represents index of letter in the guessed word
			int result = isLetterInWord(word, c); 
			if (result == 0){
				letters[0] = true;
				printWord(word, letters);
			} else if (result == 1) {
				letters[1] = true;
				printWord(word, letters);
			} else if (result == 2){
				letters[2] = true;
				printWord(word, letters);
			} else if (result == 3){
				letters[3] = true;
				printWord(word, letters);
			} else {
				misses++;
				System.out.println("Your count of misses is " + misses);
			} 
		} 
	} 
}	
